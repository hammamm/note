<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\note;
class NoteController extends Controller
{
    function index(){
      return response()->json([
          'notes' => note::all()
      ]);
    }

    function store(Request $request){
      if(note::create($request->all()) ){
        return response()->json(['status'=>'Success']);
      }
      return response()->json(['status'=>'Error']);
    }

    public function destroy(Note $note)
    {
      if($note->delete()){
        return response()->json(['status'=>'Success']);
      }
      return response()->json(['status'=>'Error']);

    }

    public function update(Request $request,Note $note)
    {
      $data = $request->validate
      ([
        'title' => 'required|max:255',
        'body' => 'required',
      ]);
      $note->title = $data['title'];
      $note->body = $data['body'];



      if($note->save()){
        return response()->json(['status'=>'Success']);
      }
      return response()->json(['status'=>'Error']);


    }
}
