<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{

    public function index()
    {
        return view('note.index');
    }


    public function getData()
    {
        return response()->json(['data'=>Note::select('id', 'title','body')->get()]);
    }


    public function store(Request $request)
    {
        if (Note::create($request->all())) {
            return response()->json(['status'=>true]);
        }
        return response()->json(['status'=>false]);
    }


    public function update(Request $request,Note $note)
    {
        if ($note->update($request->all())) {
            return response()->json(['status'=>true]);
        }
        return response()->json(['status'=>false]);
    }


    public function destroy(Note $note)
    {
        if ($note->delete()) {
            return response()->json(['status'=>true]);
        }
        return response()->json(['status'=>false]);
    }
}
