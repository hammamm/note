<div class="container">
    <!-- Button to Open the Modal -->
    <!-- The Modal -->
    <div class="modal fade" id="{{ $id }}">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"> {{ $title }}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                  {{ $inputs }}
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                  {{ $buttons }}

                </div>
            </div>
        </div>
    </div>
</div>
