<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>command #</title>
</head>

<body>
    <nav class="navbar navbar-default">
        <h1>{{__('pages/noteIndex.noteOfCommands')}}</h1>
    </nav>
    &nbsp;
    &nbsp;
    &nbsp;
    &nbsp;
    &nbsp;
    &nbsp;
    <div class="row">
        <form action="{{route('user.notestore',[$user->id])}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="Title">{{__('pages/noteCreate.title')}}</label>
                <input type="text"  id="title" aria-describedby="emailHelp" placeholder="{{__('pages/noteCreate.enterTitle')}}" name="title">

            </div>
            <div class="form-group">
                <label for="command">{{__('pages/noteCreate.command')}}</label>
                <textarea class="form-control" id="command" placeholder="{{__('pages/noteCreate.enterCommand')}}" name="body" rows="3"></textarea>
            </div>
            @include('layouts.errors')
            <div class="row">

                <button type="submit" class="btn btn-primary">{{__('pages/noteCreate.submit')}}</button>

                <a href="{{route('note.index')}}" class="btn btn-primary">{{__('pages/noteCreate.backHome')}}</a>
            </div>
        </form>
    </div>
    <div class="well well-lg">
        <h4>commands was writtn by:</h4>
        <h5>{{$user->name}}&nbsp;Email:&nbsp;{{$user->email}}</h5>
        <hr>
        <ul style="list-style-type:none">
            @foreach ($user->note as $note)
            <div class="well well-lg">
                <li>
                    <h3>{{$note->title}}</h3>
                    <h5>{{$note->body}}</h5>
                    <div class="row">
                        &nbsp;
                        &nbsp;
                        <form action="{{route('note.destroy',['id' => $note->id])}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger " >{{__('pages/noteIndex.delete')}}</button>
                        </form>
                        &nbsp;
                        &nbsp;
                        <a href="{{route('note.edit' , ['id' => $note->id])}}" class="btn btn-warning">{{__('pages/noteIndex.edit')}}</a>
                        &nbsp;
                        &nbsp;
                        <a href="{{route('note.show' , ['id' => $note->id])}}" class="btn btn-info">{{__('pages/noteIndex.show')}}</a>
                    </div>
                </li>
            </div>
            @endforeach
        </ul>
</body>
</html>
