<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>{{__('pages/noteIndex.myCommands')}}</title>
</head>

<body>
    <nav class="navbar navbar-default">
        <h1>{{__('pages/noteIndex.noteOfCommands')}}</h1>
    </nav>
    &nbsp;
    &nbsp;
    &nbsp;
    &nbsp;
    &nbsp;
    &nbsp;
    <button type="button" class="btn btn-primary btn-lg" onclick="location.href='{{route('note.create')}}'">{{__('pages/noteIndex.createNewCommand')}}</button>
    <br>
    <br>


    <ul style="list-style-type:none">
        @foreach ($users as $note)
        <div class="well well-lg">
            <li>
                <h3>{{$note->name}}</h3>
                <h5>{{$note->email}}</h5>
                <div class="row">
                    &nbsp;
                    &nbsp;
                    <form action="{{route('note.destroy',['id' => $note->id])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger " >{{__('pages/noteIndex.delete')}}</button>
                    </form>
                    &nbsp;
                    &nbsp;
                    <a href="{{route('note.edit' , ['id' => $note->id])}}" class="btn btn-warning">{{__('pages/noteIndex.edit')}}</a>
                    &nbsp;
                    &nbsp;
                    <a href="{{route('user.show' , ['id' => $note->id])}}" class="btn btn-info">{{__('pages/noteIndex.show')}}</a>
                </div>


            </li>
        </div>
        @endforeach
    </ul>
</body>

</html>
