@extends('layouts.master')
@section('title',  "{{__('pages/noteIndex.myCommands')}}")
@section('content')
   <div class="container" id="note">
        <button type="button" class="btn btn-dark btn-lg pull-right"  data-toggle="modal" data-target="#newNoteModal" v-on:click="openNewForm()">
          <i class="fa fa-plus-circle" style="font-size:16px">&nbsp;</i>
          {{__('pages/noteIndex.createNewCommand')}}
        </button>
        <h1 class="my-4 text-center text-lg-left">All Commands</h1>
        <div class="row text-center text-lg-left">
            <div class="card" style="width: 25rem;" id='noteTitle' v-for="(note,index) in noteArray">
                <div class="card-body">
                <h3 class="card-title">@{{note.title}}</h3>
                    <p class="card-text">@{{ (substr(note.body,0,200) ) }}</p>
                    <div class="row" style="position: static; bottom:0px;">
                        &nbsp;&nbsp;
                        <button  class="btn btn-danger" v-on:click="deleteeNote(index)"><i class="material-icons" style="font-size:20px">delete</i></button>


                        &nbsp;&nbsp;
                        <button v-on:click="openModifyForm(index);modifyStatus = true"class="btn btn-info" data-toggle="modal" data-target="#noteEditModal"><i class="material-icons" style="font-size:20px">edit</i>Edit</button>

                        &nbsp;&nbsp;
                        <a href="" class="btn btn-info"><i class="fa fa-inbox" style="font-size:20px"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
        @component('layouts.formModal')
    @slot('id',"newNoteModal")
    @slot('title',"Add New Note")
    @slot('inputs')
        <div class="form-group">
            <label>title</label>
            <input type="text"class="form-control" v-model="title">
        </div>
        <div class="form-group">
            <label>body</label>
            <input type="text"class="form-control" v-model="body">
        </div>
    @endslot

    @slot('buttons')
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" v-on:click="storeNote()" v-if="!modifyStatus && !showNote">Save</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" v-on:click="editNote(id)" v-if="modifyStatus && !showNote">Update</button>
    @endslot
@endcomponent

</div>
@endsection('content')
@section('script')
@include('note.script.index')
@endsection('script')
