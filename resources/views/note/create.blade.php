@extends('layouts.master')
@section('title',"{{__('pages/noteCreate.createNewTitle')}}")
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <form action="{{route('note.store')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="Title">{{__('pages/noteCreate.title')}}</label>
                    <input type="text" class="form-control" id="title" aria-describedby="emailHelp" placeholder="{{__('pages/noteCreate.enterTitle')}}" name="title">

        </div>
                <div class="form-group">
                    <label for="command">{{__('pages/noteCreate.command')}}</label>
                    <textarea class="form-control" id="command" placeholder="{{__('pages/noteCreate.enterCommand')}}" name="body" rows="10"></textarea>
                </div>
                @include('layouts.errors')
                <div class="row">
                    &nbsp;&nbsp;
                    <button type="submit" class="btn btn-dark">{{__('pages/noteCreate.submit')}}</button>
                    &nbsp;&nbsp;
                    <a href="{{route('note.index')}}" class="btn btn-dark">{{__('pages/noteCreate.backHome')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection('content')
