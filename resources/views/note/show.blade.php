<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>command #{{$note->id}}</title>
</head>

<body>
    <nav class="navbar navbar-default">
        <h1>{{__('pages/noteIndex.noteOfCommands')}}</h1>
    </nav>
    <div class="well well-lg">
        <h4>commands was writtn by:</h4>
        <h5>{{$note->user->name}}&nbsp;Email:&nbsp;{{$note->user->email}}</h5>
        <hr>
        <h3>{{$note->title}}</h3>
        <h5>{{$note->body}}</h5>
        <div class="row">
            &nbsp;
            &nbsp;
            <form action="{{route('note.destroy',['id' => $note->id])}}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button class="btn btn-danger " >{{__('pages/noteIndex.delete')}}</button>
            </form>
            &nbsp;
            &nbsp;
            <a href="{{route('note.edit' , ['id' => $note->id])}}" class="btn btn-warning">{{__('pages/noteIndex.edit')}}</a>

        </div>


    </div>
</body>

</html>
