<script>
  var note = new Vue({
      el: '#note',
      data: {
          title: '',
          body: '',
          modifyStatus: [],
          noteArray: [],
          token: '{{csrf_token()}}'
      },
      created() {
          this.getAllNote();
      },
      methods: {
          getAllNote() {
              this.$http.get('/mo/note/getData').then(result => {
                  this.noteArray = [];
                  for (var i = 0; i < result.body.data.length; i++) {
                      this.noteArray.push({
                          id: result.body.data[i].id,
                          title: result.body.data[i].title,
                          body: result.body.data[i].body,
                          modifyStatus: false
                      });
                  }
              });
          },
          deleteNote(id) {
              this.$http.delete('/mo/note/' + this.noteArray[id].id, {
                  params: {
                      '_token': this.token
                  },
                  headers: {
                      'X-CSRF-TOKEN': this.token
                  }
              }).then(result => {
                  if (result.body.status) {
                      this.getAllNote();
                  }
              });
          },
          editNote(id) {
              this.$http.put('/mo/note/' + this.noteArray[id].id, {
                  '_token': this.token,
                  title: this.noteArray[id].title
                  body: this.noteArray[id].body
              }).then(result => {
                  if (result.body.status) {
                      this.getAllNote();
                  }

              });
          },
          storeNote() {
              this.$http.post('/mo/note', {
                  '_token': this.token,
                  title: this.title
                  body: this.body
              }).then(result => {
                  if (result.body.status) {
                      this.title = '';
                      this.body = '';
                      this.getAllNote();
                  }
              });
          },
      }
  });
</script>
