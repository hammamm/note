<?php
  return
  [
    'createNewTitle' => 'Create new note',
    'title' => 'Title:',
    'command' => 'Command:',
    'enterCommand' => 'Enter your command here',
    'submit' => 'Submit',
    'enterTitle' => 'Enter title',
    'backHome' => 'Back Home'
  ];
?>
