<?php
  return
  [
    'myCommands' => 'My Commands',
    'noteOfCommands' => 'Note of Commands',
    'delete' => 'Delete',
    'createNewCommand' => 'Create new command',
    'edit' => 'Edit',
    'show' => 'Show'
  ];
?>
