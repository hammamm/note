<?php

Route::resource('/note','NoteController');
Route::resource('user','UserController');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::post('user/notestore/{user}','UserController@notestore')->name('user.notestore');
